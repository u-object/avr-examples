/*
 * @author Vlad Vetrov
 * ufo.cool@gmail.com
 */
#ifndef MAIN_H
#define MAIN_H

#define RS_ENABLE PORTA |= (1<<PINA0);
#define RS_DISABLE PORTA &= ~(1<<PINA0);
#define EN_ENABLE PORTA |= (1<<PINA1);
#define EN_DISABLE PORTA &= ~(1<<PINA1);
#define LCD_PORT PORTA
#define SPEAKER_PORT PINA2

void beep(void);
void lcd_write_str(char *str);
void init_display(void);
void lcd_write_nibble(int data);
void lcd_cmd(int data);
void lcd_clear(void);
void init_ADC(void);
unsigned int read_ADC(uint8_t channel);
void right (void);
void shift_right(unsigned int);
void shift_left(unsigned int);
void lcd_home(void);
void lcd_cursor_pos(unsigned int line, unsigned int pos);

#endif /* MAIN_H */

