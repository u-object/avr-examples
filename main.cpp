/*
 * @author Vlad Vetrov
 * ufo.cool@gmail.com
 */
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "main.h"

int z = 0;
//int adc_result;
char str[4];
int main(void){ 
/***************************************************************************************************    
//     Port A initialization
//     Function: Bit7=Out Bit6=Out Bit5=Out Bit4=Out Bit3=In Bit2=Out Bit1=Out Bit0=Out
 ****************************************************************************************************/
    DDRA=(1<<DDA7) | (1<<DDA6) | (1<<DDA5) | (1<<DDA4) | (0<<DDA3) | (1<<DDA2) | (1<<DDA1) | (1<<DDA0);
    PORTA=(0<<PORTA7) | (0<<PORTA6) | (0<<PORTA5) | (0<<PORTA4) | (0<<PORTA3) | (0<<PORTA2) | (0<<PORTA1) | (0<<PORTA0);
/***************************************************************************************************
//     Port D initialization
//     Function: Bit3=In Bit2=In
****************************************************************************************************/
    DDRD=(0<<DDD2) | (0<<DDD3) | (0<<DDD4) | (1<<DDD6);
    PORTD=(1<<PORTD2) | (1<<PORTD3) | (1<<PORTD4) | (1<<PORTD6);
/***************************************************************************************************
//     Port B initialization
//     Function: Bit3=In Bit2=In
****************************************************************************************************/
    DDRB = (1<<DDB0) | (1<<DDB1) | (1<<DDB2) | (1<<DDB3);
    PORTB=(0<<PORTB0) | (0<<PORTB1) | (0<<PORTB2) | (0<<PORTB3);
/***************************************************************************************************
//     External Interrupt(s) initialization
//     INT0: On
//     INT0 Mode: The low level
//     INT1: on
//     INT1 Mode: Rising Edge
//     INT2: Off
****************************************************************************************************/
    GICR=(0<<INT1) | (0<<INT0);
    MCUCR=(1<<ISC11) | (1<<ISC10) | (0<<ISC01) | (0<<ISC00);
    init_display();
    lcd_cursor_pos(1,5);
    lcd_write_str("Hello");
    lcd_cursor_pos(2,5);
    lcd_write_str("Vlad");
    //init_ADC();
    //int L = 0;
    //sei();
    _delay_ms(2000);
    while (1){
//        right();
//        if(L==199) PORTB = 0b00000000;
//        L++;
//        lcd_clear();
//        sprintf(str, "%d", L);
//        lcd_write_str(str);
        
//        lcd_clear();
//        sprintf(str, "%d", read_ADC(3));
//        lcd_write_str(str);
//        _delay_ms(100);
        
//        PORTA |= 1<<PINA0;
//        _delay_ms(500);
//        PORTA &= ~1<<PINA0;
//        _delay_ms(500);
    }
}

/************************************************************************/
/*  Stepper Motor                                                       */
/************************************************************************/
void right (void) {
        PORTB = 0b00001000;
        _delay_ms(2);
        PORTB = 0b00001100;
        _delay_ms(2);
        PORTB = 0b00000100;
        _delay_ms(2);
        PORTB = 0b00000110;
        _delay_ms(2);
        PORTB = 0b00000010;
        _delay_ms(2);
        PORTB = 0b00000011;
        _delay_ms(2);
        PORTB = 0b00000001;
        _delay_ms(2);
        PORTB = 0b00001001;
        _delay_ms(2);	
}

// External Interrupt
ISR(INT0_vect) {
    //char str[4];
    if((((PIND) & (1<<PD2)) == 0) ^ (((PIND) & (1<<PD4)) == 0))
    {
        z++;
    }else{
        z--;
    }
    lcd_clear();
    sprintf(str, "%d", z);
    lcd_write_str(str);
    _delay_ms(30);
}

ISR(INT1_vect){
    PORTD ^= 1<<PIND6;
    beep();
}
void beep(void) {
    int a;
    for(a=0; a<100; a++) {
         PORTA ^= 1 << SPEAKER_PORT;
         _delay_us(100);
    }
    PORTA &= ~1<<PINA2;
}
/***************************************************************************************************
*   send string to  LCD
****************************************************************************************************/
 void lcd_write_str(char *str) {
    do {
        lcd_cmd(*str);
    }while(*++str);
}

/***************************************************************************************************
*   initial LCD
****************************************************************************************************/
void init_display(void) {
    RS_DISABLE;
	//PORTA = (0<<PORTA0);
    _delay_ms(100);
    
    lcd_write_nibble(0x30);
    _delay_ms(2);  
//              
    lcd_write_nibble(0x30);
    _delay_ms(2);
//
    lcd_write_nibble(0x20);
    _delay_ms(2);
//        
    lcd_cmd(0x28);   //set data length 4 bit 2 line
    _delay_ms(2);    
//
    lcd_cmd(0x0E);   // set display on, cursor on, blink off
    _delay_ms(2);
//
    lcd_clear();
            
    RS_ENABLE;  //rs = 1
	//PORTA = (1<<PORTA0);    
}
/***************************************************************************************************
*   8bit command LCD
****************************************************************************************************/
void lcd_write_nibble(int data) {
    PORTA |= data;
    EN_ENABLE;
    _delay_us(500);
    EN_DISABLE;  
    PORTA &= ~(data);   
} 

/***************************************************************************************************
*   4bit command LCD
****************************************************************************************************/
void lcd_cmd(int data) {
    lcd_write_nibble(data & 0xF0);
    lcd_write_nibble(data << 4);
}
/***************************************************************************************************
*   clear LCD
****************************************************************************************************/
void lcd_clear(void) {
     RS_DISABLE;
     lcd_cmd(0x01); 
     _delay_ms(1); 
     RS_ENABLE;
}
/***************************************************************************************************
*   shift right LCD
****************************************************************************************************/
void shift_right(unsigned int step) {
    unsigned int i;
     RS_DISABLE;
     for(i=0;i<step;i++) {
        lcd_cmd(0b00010100); 
        _delay_ms(1); 
     }
     RS_ENABLE;
}
/***************************************************************************************************
*   shift left LCD
****************************************************************************************************/
void shift_left(unsigned int step) {
    unsigned int i;
     RS_DISABLE;
     for(i=0;i<step;i++) {
        lcd_cmd(0b00010000); 
        _delay_ms(1); 
     }
     RS_ENABLE;
}
/***************************************************************************************************
*   LCD set cursor position
*   pos: 1-16, line: 1-2
****************************************************************************************************/
void lcd_cursor_pos(unsigned int line, unsigned int pos) {
    unsigned int i;
    RS_DISABLE;
    lcd_cmd(0b00000010); 
    _delay_ms(1);
    if(line == 1) {
        lcd_cmd(0b00000010); 
        _delay_ms(1);
    }else if(line == 2){
        lcd_cmd(0b01000000);
        _delay_ms(1); 
    }

    for(i=0;i<pos;i++) {
        lcd_cmd(0b00010100); 
        _delay_ms(1); 
     }
     RS_ENABLE;
}
/***************************************************************************************************
*   cursor Home LCD
****************************************************************************************************/
void lcd_home(void) {
     RS_DISABLE;
    lcd_cmd(0b00000010); 
    _delay_ms(1); 
     RS_ENABLE;
}
/***************************************************************************************************	
    ADC - Аналого цифровой преобразователь
    1.ADMUX - Регистр управления мультиплексором ADC 
            REFS1:0 - Биты выбора источника опорного напряжения 
            ADLAR - Бит управления представлением результата преобразования 
            MUX4:0 - Биты выбора аналогового канала и коэффициента усиления
    2.ADCSRA - Регистр управления и статуса ADC
            ADEN - Разрешение работы ADC 
            ADSC -Запуск преобразования ADC 
            ADATE - Включение режима автоматического запуска ADC 
            ADIF - Флаг прерывания
            ADIE - Разрешение прерывания ADC
            ADPS2:0 - Биты управления предделителем ADC
    3.ADCL и ADCH - Регистры данных ADC
****************************************************************************************************/
void init_ADC(void) {
	ADCSRA = (1<<ADEN) | (0<<ADSC) | (0<<ADATE)| (0<<ADIF) | (0<<ADIE) | (1<<ADPS2) |(1<<ADPS1) |(1<<ADPS0);
	ADMUX = (0<<REFS1) | (1<<REFS0) | (0<<ADLAR) | (0<<MUX4) | (0<<MUX3) | (0<<MUX2) | (1<<MUX1) | (1<<MUX0);
}

unsigned int read_ADC(uint8_t channel) {
    ADCSRA |= (1<<ADSC); // Start conversion
    while (ADCSRA & (1<<ADSC)); // wait for conversion to complete
    return(ADCW); //Store ADC value
}
